export function isDataEmpty(obj) {
  let empty = false

  Object.keys(obj).forEach((key) => {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      if (obj[key] === '') {
        empty = true
      }
    }
  })

  return empty
}
