import React from "react";
import '../css/App.css'
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import List from "./List";
import Edit from "./Edit";
import Create from "./Create";

export default function App() {
  return (
    <Router>
      <div>
        <nav className="navbar navbar-light bg-light">
          <div className="container">
            <span className="navbar-brand mb-0 h1">Students client</span>
          </div>
        </nav>
        <Switch>
          <Route exact path="/">
            <List />
          </Route>
          <Route exact path="/create">
            <Create />
          </Route>
          <Route path="/edit/:id">
            <Edit />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}
