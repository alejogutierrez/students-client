import { Link } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { deleteStudentById } from '../store/StudentsSlice'

const ListItem = ({ student }) => {
  const dispatch = useDispatch()

  const deleteElem = () => {
    dispatch(deleteStudentById(student.id))
  }

  return (
    <tr>
      <td>{student.id}</td>
      <td>{student.username}</td>
      <td>{student.firstName}</td>
      <td>{student.lastName}</td>
      <td>{student.age}</td>
      <td>{student.career}</td>
      <td>
        <Link to={'/edit/'+student.id}>
          <button type="button" className="btn btn-action btn-primary">Edit</button>
        </Link>
        <button type="button" className="btn btn-action btn-danger" onClick={deleteElem}>Delete</button>
      </td>
    </tr>
  )
}

export default ListItem
