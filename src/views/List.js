import React from 'react'
import { Link } from 'react-router-dom'
import { useSelector } from 'react-redux'
import ListItem from './ListItem'

export default function List() {
  const studentsData = useSelector(state => state)
  const renderedList = studentsData.map(item => {
    return <ListItem key={item.id} student={item}></ListItem>
  })

  return (
    <div className="container page-content">
      <div className="row justify-content-start">
        <div className="col-4 text-start">
          <Link to="create">
            <button type="button" className="btn btn-success">Create</button>
          </Link>
        </div>
      </div>
      <hr></hr>
      <div className="row">
        <div className="table-responsive">
          <table className="table">
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Username</th>
                <th scope="col">FirstName</th>
                <th scope="col">LastName</th>
                <th scope="col">Age</th>
                <th scope="col">Career</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
              { renderedList }
            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}
