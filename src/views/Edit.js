import { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useParams, useHistory, Link } from "react-router-dom"
import { updateStudent } from '../store/StudentsSlice'
import { isDataEmpty } from '../utils/validation'

export default function Edit() {
  const { id } = useParams()
  const dispatch = useDispatch()
  const history = useHistory()
  const studentsData = useSelector(state => state)
  let student = studentsData.find(item => Number(item.id) === Number(id))

  if (!student) {
    student = {
      firstName: '',
      lastName: '',
      username: '',
      age: '',
      career: ''
    }
  }

  const [ firstName, setFirstName ] = useState(student.firstName)
  const [ lastName, setLastName ] = useState(student.lastName)
  const [ username, setUsername ] = useState(student.username)
  const [ age, setAge ] = useState(student.age)
  const [ career, setCareer ] = useState(student.career)
  const [ message, setMessage ] = useState('')



  const updateUsername = () => {
    if (firstName !== '' && lastName !== '') {
      const transformedFirst = firstName.toLowerCase().replace(' ', '.')
      const transformedLast = lastName.toLowerCase().replace(' ', '.')

      setUsername(transformedFirst + '.' + transformedLast)
    }
  }

  const handleFirstName = (event) => {
    setFirstName(event.target.value)
  }

  const handleLastName = (event) => {
    setLastName(event.target.value)
  }

  const handleAge = (event) => {
    setAge(event.target.value)
  }

  const handleCareer = (event) => {
    setCareer(event.target.value)
  }

  const handleSubmit = (event) => {
    event.preventDefault()

    const editedStudent = {
      id: Number(id),
      firstName,
      lastName,
      username,
      age,
      career
    }

    if (isDataEmpty(editedStudent)) {
      setMessage('There are some empty fields. Please complete the form to send!')
    } else {
      dispatch(updateStudent(editedStudent))
      history.push('/')
    }
  }

  const hasErrMessage = () => {
    if (message !== '') {
      return <div className="alert alert-danger" role="alert">
              {message}
            </div>
    }
  }

  return (
    <div className="container page-content">
      <div className="row">
        <div className="col text-start">
          <Link to="/">Go back</Link>
        </div>
      </div>
      <div className="row justify-content-center">
        <div className="col-md-6">
          <form onSubmit={handleSubmit}>
            <div className="mb-3 text-start">
              <label className="form-label">UserName</label>
              <input type="text" id="userName" className="form-control" value={username} readOnly />
            </div>
            <div className="mb-3 text-start">
              <label className="form-label">First Name</label>
              <input type="text" id="firstName" className="form-control" value={firstName} onChange={handleFirstName} onBlur={updateUsername} />
            </div>
            <div className="mb-3 text-start">
              <label className="form-label">Last Name</label>
              <input type="text" id="lastName" className="form-control" value={lastName} onChange={handleLastName} onBlur={updateUsername}/>
            </div>
            <div className="mb-3 text-start">
              <label className="form-label">Age</label>
              <input type="number" min="1" id="userName" className="form-control" value={age} onChange={handleAge}/>
            </div>
            <div className="mb-3 text-start">
              <label className="form-label">Career</label>
              <input type="text" id="Career" className="form-control" value={career} onChange={handleCareer}/>
            </div>
            <div className="mb-3 text-start">
              {hasErrMessage()}
            </div>
            <button type="submit" className="btn btn-primary">Update</button>
          </form>
        </div>
      </div>
    </div>
  )
}
