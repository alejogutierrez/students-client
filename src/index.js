import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './views/App';
import store from './store/StudentsStore'
import { Provider } from 'react-redux'
import { fetchStudents } from './store/StudentsSlice'

store.dispatch(fetchStudents)

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

