import { getStudentsReq, createStudent, editStudent, deleteStudent } from '../api/requests/students'

const initialState = []

export default function studentsReducer(state = initialState, action) {
  switch (action.type) {
    case 'students/studentAdded': {
      return [...state, action.payload]
    }
    case 'students/studentUpdated': {
      const { username, firstName, lastName, age, career, id } = action.payload
      return state.map((student) => {
        if (student.id !== id) {
          return student
        }

        return {
          ...student, username, firstName, lastName, age, career
        }
      })
    }
    case 'students/studentsLoaded': {
      return action.payload
    }
    case 'students/studentDeleted': {
      return state.filter((student) => student.id !== action.payload)
    }
    default:
      return state
  }
}

// Thunk function
export async function fetchStudents(dispatch, getState) {
  const response = await getStudentsReq()
  dispatch({ type: 'students/studentsLoaded', payload: response.data })
}

export function saveStudent(student, lastIdx) {
  return async function saveNewStudent(dispatch, getState) {
    const response = await createStudent(student, lastIdx)
    dispatch({ type: 'students/studentAdded', payload: response.data })
  }
}

export function updateStudent(student) {
  return async function saveEditStudent(dispatch, getState) {
    await editStudent(student)
    console.log(dispatch({ type: 'students/studentUpdated', payload: student }))
  }
}

export function deleteStudentById(id) {
  return async function deleteSelStudent(dispatch, getState) {
    await deleteStudent(id)
    dispatch({ type: 'students/studentDeleted', payload: id })
  }
}
