import axios from 'axios'
import { setupMockedRoutes } from './routes.js'

let service = null

function getServiceInstance() {
  if (!service) {
    service = axios.create({
      baseURL: process.env.REACT_APP_BASE_API
    })

    setupMockedRoutes()
  }

  return service
}

export {
  getServiceInstance
}
