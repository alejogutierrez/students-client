import { getRoute } from '../routes.js'
import * as students from './students.js'

const mockedRoutes = []

function addRouteToMock(routeName) {
  if (mockedRoutes.indexOf(routeName) === -1) {
    mockedRoutes.push(routeName)
  }
}

function mockResponse(config) {
  if (config.url === getRoute('list-students').url  && config.method === getRoute('list-students').method) {
    return students.mockList()
  }

  if (config.url === getRoute('create-student').url && config.method === getRoute('create-student').method) {
    return students.mockCreate(config)
  }

  if (config.url === getRoute('update-student').url && config.method === getRoute('update-student').method) {
    return students.mockEdit(config)
  }

  if (new RegExp(getRoute('delete-student').url + '/*').test(config.url)) {
    return students.mockDelete(config)
  }
}

function checkInterceptors(service, routeName) {
  if (mockedRoutes.indexOf(routeName) !== -1) {
    service.interceptors.request.use(config => {
      config.baseURL = 'local.dev'
      config.adapter = (config) => {
        return mockResponse(config)
      }

      return config
    })
  }
}

export {
  addRouteToMock,
  checkInterceptors
}
