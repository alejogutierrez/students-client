// import store from '../../store/StudentsStore'
import * as students from './data/students.json'

export function mockList(config) {
  return new Promise(function(resolve) {
    var response = {
      data: students.default,
      status: 200,
      statusText: 'OK'
    }

    setTimeout(() => {
      resolve(response)
    }, 1000)
  })
}

export function mockCreate(config) {
  return new Promise(function(resolve) {
    const newStudent = JSON.parse(config.data)
    newStudent.id = config.params.lastIdx

    var response = {
      data: newStudent,
      status: 200,
      statusText: 'OK'
    }

    setTimeout(() => {
      resolve(response)
    }, 1000)
  })
}

export function mockEdit(config) {
  return new Promise(function(resolve) {
    var response = {
      data: 'Student record updated.',
      status: 200,
      statusText: 'OK'
    }

    setTimeout(() => {
      resolve(response)
    }, 1000)
  })
}

export function mockDelete(config) {
  return new Promise(function(resolve) {
    var response = {
      data: 'Student record deleted successfully.',
      status: 200,
      statusText: 'OK'
    }

    setTimeout(() => {
      resolve(response)
    }, 1000)
  })
}
