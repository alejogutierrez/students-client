import { getServiceInstance } from '../index.js'
import { getRoute } from '../routes.js'
import { checkInterceptors } from '../mock/index.js'

export function getStudentsReq() {
  const service = getServiceInstance()

  checkInterceptors(service, 'list-students')

  return service({
    method: getRoute('list-students').method,
    url: getRoute('list-students').url,
    responseType: 'json'
  })
}

export function createStudent(student, lastIdx) {
  const service = getServiceInstance()

  checkInterceptors(service, 'create-student')

  return service({
    params: {
      lastIdx
    },
    data: student,
    method: getRoute('create-student').method,
    url: getRoute('create-student').url,
    responseType: 'json'
  })
}

export function editStudent(student) {
  const service = getServiceInstance()

  checkInterceptors(service, 'update-student')

  return service({
    data: student,
    method: getRoute('update-student').method,
    url: getRoute('update-student').url,
    responseType: 'json'
  })
}

export function deleteStudent(id) {
  const service = getServiceInstance()

  checkInterceptors(service, 'delete-student')

  return service({
    method: getRoute('delete-student').method,
    url: getRoute('delete-student').url + '/' + id,
    responseType: 'json'
  })
}
