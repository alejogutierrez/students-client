import { addRouteToMock } from './mock/index.js'

const routes = [
  {
    name: 'list-students',
    url: '/student',
    method: 'get'
  },
  {
    name: 'create-student',
    url: '/student',
    method: 'post'
  },
  {
    name: 'update-student',
    url: '/student',
    method: 'put'
  },
  {
    name: 'delete-student',
    url: '/student',
    method: 'delete'
  }
]

function getRoute(routeName) {
  const route = routes.find(item => item.name === routeName)

  if (route) {
    return route
  }

  return {
    url: ''
  }
}

function setupMockedRoutes() {
  if (process.env.NODE_ENV === 'development') {
    routes.forEach(item => {
      addRouteToMock(item.name)
    })
  }
}

export {
  getRoute,
  setupMockedRoutes
}
